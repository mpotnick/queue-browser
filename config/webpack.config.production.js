import webpack from 'webpack';
import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';

import baseConfig from './webpack.config.base';

const config = {
  ...baseConfig,

  // devtool: 'source-map',

  entry: {
    render: './src/render',
    bridge: './src/render/bridge',
  },

  output: {
    path: path.resolve(__dirname, '..', 'app'),
    filename: '[name].[hash].js'
  },

  plugins: [
    ...baseConfig.plugins,
    new webpack.DefinePlugin({
      __DEV__: false,
    }),
    new webpack.ExtendedAPIPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        screw_ie8: true,
        warnings: false,
        // drop_console: true,
      },
      output: {
        comments: false,
      },
    }),
    new ExtractTextPlugin('render.[hash].css'),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, '..', 'src', 'render', 'index.html'),
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true,
      },
    }),

  ],

  target: 'electron-renderer'
};

export default config;
