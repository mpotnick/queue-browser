import webpack from 'webpack';
import path from 'path';
import baseConfig from './webpack.config.base';

export default {
  ...baseConfig,

  // devtool: 'source-map',

  entry: {
    main: './src/main',
  },

  output: {
    path: path.resolve(__dirname, '..', 'app'),
    filename: '[name].js'
  },

  plugins: [
    ...baseConfig.plugins,
    // new webpack.optimize.UglifyJsPlugin({
    //   compressor: {
    //     warnings: false
    //   }
    // }),
    // new webpack.BannerPlugin(
    //   'require("source-map-support").install();',
    //   { raw: true, entryOnly: false }
    // ),
    // new webpack.ExternalsPlugin('commonjs', ['electron'])
    new webpack.DefinePlugin({
      __DEV__: false,
    })
  ],

  target: 'electron',

  node: {
    __dirname: false,
    __filename: false
  },

  externals: [
    ...baseConfig.externals,
    // 'font-awesome',
    // 'source-map-support'
  ]
};
