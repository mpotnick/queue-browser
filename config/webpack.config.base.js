import path from 'path';
import webpack from 'webpack';
import autoprefixer from 'autoprefixer';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

import pkg from '../package.json';

export default {
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loaders: [
          'ng-annotate?{"map": false}',
          'babel-loader?{"cacheDirectory": true}',
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
      {
        test: /\.css$/,
        loader: process.env.NODE_ENV === 'production' ? ExtractTextPlugin.extract('style', 'css', 'postcss') : 'style!css?sourceMap!postcss',
      },
      {
        test: /\.scss$/,
        loader: process.env.NODE_ENV === 'production' ? ExtractTextPlugin.extract('style', 'css!postcss!sass') : 'style!css?sourceMap!sass?sourceMap!postcss',
      },
      {
        test: /\.html$/,
        loader: 'html?attrs=false',
      },
      {
        test: /\.(png|jpg|gif|woff2?|eot|ttf)(\?.*)?$/,
        loader: 'url',
        query: {
          limit: 60000,
          context: path.resolve(__dirname, '..', 'src'),
          name: '[path][name].[hash:8].[ext]',
        },
      },
      {
        test: /\.svg$/,
        loader: 'svg-sprite',
        query: {
          name: '[name].[hash]',
          prefixize: true,
        },
      },
    ]
  },
  output: {
    path: path.resolve(__dirname, '../app'),
    filename: '[name].js',
    libraryTarget: 'commonjs2',
  },

  resolve: {
    extensions: ['', '.js', '.jsx'],
    packageMains: ['webpack', 'browser', 'web', 'browserify', ['jam', 'main'], 'main'],
    fallback: [
      path.resolve(__dirname, '..', 'node_modules'),
    ],
  },

  postcss() {
    return [autoprefixer];
  },

  sassLoader: {
    includePaths: path.resolve(__dirname, '..', 'src'),
    outputStyle: 'nested',
  },

  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),

    new webpack.ProvidePlugin({
      fetch: 'imports?this=>global!exports?global.fetch!whatwg-fetch',
    }),

    new webpack.DefinePlugin({
      'process.env.DEBUG': JSON.stringify(process.env.DEBUG),
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      APP_NAME: JSON.stringify(pkg.name),
      // APP_ID: JSON.stringify(pkg.applicationId),
      APP_VERSION: JSON.stringify(pkg.version),
    }),
  ],
  externals: [
    // put your node 3rd party libraries which can't be built with webpack here
    // (mysql, mongodb, and so on..)
  ],
};
