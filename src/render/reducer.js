/*
 * -----------------------------------------------------------------------------
 * Reducer Combiner
 * -----------------------------------------------------------------------------
 *
 * Reducers are a crucial part of this application, and this is where they're
 * all combined into one giant reducer. The CoreReducers don't need to be
 * modified, and they need to stay here. They enable all of the layout
 * actions. Add any page reducers you need after the core reducers.
 *
 */

import { combineReducers } from 'redux';
import { reduceReducers } from 'cx-redux-utils';
import { browserWindow } from 'cx-electron-utils/lib/redux/modules/browserWindow';
import { ipc } from 'cx-electron-utils/lib/redux/modules/ipc';
import { webContents } from 'cx-electron-utils/lib/redux/modules/webContents';

import CoreReducers from './global/core/core.reducers';
import { app } from './redux/modules/app';
import { config } from './redux/modules/config';

const namedReducers = combineReducers({
  ...CoreReducers,
  browserWindow,
  ipc,
  webContents,
  app,
  config,
});

export default reduceReducers(
  namedReducers,
);
