import { name, productName, version } from '../../../../package.json';

const { platform, arch } = process;
export default {
  name,
  productName,
  version,
  platform,
  arch,
  userAgent: null,
  isUpdateDownloaded: false,
};
