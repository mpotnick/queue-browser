import {
  createAction,
  createErrorAction,
  createSetter,
  createReducer,
} from 'cx-redux-utils';
import defaultState from './state';

export const SET_IS_UPDATE_DOWNLOADED = '@@app/setIsUpdateDownloaded';
export const setIsUpdateDownloaded = createAction(SET_IS_UPDATE_DOWNLOADED);
function setIsUpdateDownloadedHandler(state, action) {
  const isUpdateDownloaded = action.payload;
  return {
    isUpdateDownloaded,
  };
}

const actionMap = {
  [SET_IS_UPDATE_DOWNLOADED]: setIsUpdateDownloadedHandler,
};

export const app = createReducer(defaultState, actionMap);
