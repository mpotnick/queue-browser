import {
  createAction,
  createErrorAction,
  createSetter,
  createReducer,
} from 'cx-redux-utils';
import { bind } from 'redux-effects';
import { fetch } from 'redux-effects-fetch';
import electronJsonStorage from 'electron-json-storage';
import os from 'os';

import defaultState from './state';
import pkg from '../../../../package.json';

export const SET_CONFIG = '@@config/setConfig';
export const setConfig = createAction(SET_CONFIG);
function setConfigHandler(state, action) {
  return {
    ...action.payload,
  };
}

export const SAVE_CONFIG = '@@config/saveConfig';
const saveConfig = createAction(SAVE_CONFIG);
function saveConfigHandler(state, action) {
  electronJsonStorage.get(`${pkg.name}.config`, (getErr, config) => {
    if (getErr) {
      // todo: dispatch config save error
    }
    electronJsonStorage.set(
      `${pkg.name}.config`,
      {
        ...config,
        ...action.payload,
      },
      (setErr) => {
        if (setErr) {

        }
        // todo: dispatch config save success
    });
  });
}

export const checkDomainPrefix = (domainPrefix) => {
  return (dispatch, getState) => {
    return dispatch(checkDomainUri(domainPrefix))
      .then(() => {
        if (getState().config.domainIsValid) {
          dispatch(setDomainPrefix(domainPrefix));
          dispatch(saveConfig(getState().config));
        }
      });
  };
};

export const SET_DOMAIN_PREFIX = '@@config/setDomainPrefix';
const setDomainPrefix = createAction(SET_DOMAIN_PREFIX);
function setDomainPrefixHandler(state, action) {
  return {
    domainPrefix: action.payload
  };
}

export const GET_DOMAIN_URI_SUCCESS = '@@config/getDomainUriSuccess';
export const getDomainUriSuccess = createAction(GET_DOMAIN_URI_SUCCESS);
export function getDomainUriSuccessHandler(state, action, meta) {
  if (action.payload.status === 200) {
    return {
      ...state,
      domainIsValid: true,
    };
  }
  return {
    ...state
  };
}

export const GET_DOMAIN_URI_ERROR = '@@config/getDomainUriError';
export const getDomainUriError = createErrorAction(GET_DOMAIN_URI_ERROR, 'Queue ID did not resolve to a valid domain.');
function getDomainUriErrorHandler(state, action) {
  return {
    ...state,
    domainIsValid: false,
  };
}

export function checkDomainUri(domain) {
  const uri = `https://${domain}.crosschx.com/${pkg.name}/api`;
  return bind(
    fetch(uri),
    getDomainUriSuccess,
    getDomainUriError,
  );
}

const actionMap = {
  [SET_CONFIG]: setConfigHandler,
  [SAVE_CONFIG]: saveConfigHandler,
  [SET_DOMAIN_PREFIX]: setDomainPrefixHandler,
  [GET_DOMAIN_URI_ERROR]: getDomainUriErrorHandler,
  [GET_DOMAIN_URI_SUCCESS]: getDomainUriSuccessHandler,
};

export const config = createReducer(defaultState, actionMap);

export default {
  checkDomainPrefix,
  checkDomainUri,
  setConfig,
};
