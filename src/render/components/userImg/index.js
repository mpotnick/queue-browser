import angularMd5 from 'angular-md5';

import { ONE_WAY } from '../../global/bindings';
import QueueIcon from '../../global/core/icon';

import template from './userImg.html';

import './userImg.scss';

const userImgComponent = {
  bindings: {
    emailAddress: ONE_WAY,
  },
  template,
}

export default angular
  .module('queueBrowser.components.userImg', [
    QueueIcon,
    angularMd5,
  ])
  .component('cxUserImg', userImgComponent)
  .name;
