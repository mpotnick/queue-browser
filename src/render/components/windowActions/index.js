import angular from 'angular';

import windowActionsComponent from './windowActions.component';
import QueueIcon from '../../global/core/icon';

export default angular
  .module('queueBrowser.components.windowActions', [
    QueueIcon,
  ])
  .component('cxWindowActions', windowActionsComponent)
  .name;
