import { CALLBACK, ONE_WAY } from '../../global/bindings';
import template from './windowActions.html';

export default {
  template,
  bindings: {
    isMaximized: ONE_WAY,
    handleClickCloseButton: CALLBACK,
    handleClickIndeterminateButton: CALLBACK,
    handleClickMaximizeButton: CALLBACK,
    handleClickMinimizeButton: CALLBACK,
  },
};
