import angular from 'angular';

import appBarComponent from './appBar.component';
import QueueIcon from '../../global/core/icon';
import cxUserImg from '../userImg';
import cxWindowActions from '../windowActions';

export default angular
  .module('queueBrowser.components.appBar', [
    QueueIcon,
    cxUserImg,
    cxWindowActions,
  ])
  .component('cxAppBar', appBarComponent)
  .name;
