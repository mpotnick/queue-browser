import template from './appBar.html';
import controller from './appBar.controller';

export default {
  template,
  controller,
};
