import {
  maximizeWindow,
  minimizeWindow,
  restoreWindow,
  closeWindow,
} from 'cx-electron-utils/lib/redux/modules/browserWindow';

import {
  goForward,
  goBack,
} from 'cx-electron-utils/lib/redux/modules/webContents';

import './appBar.scss';

const actions = {
  goBack,
  goForward,
  maximizeWindow,
  minimizeWindow,
  restoreWindow,
  closeWindow,
};

export default class appBarController {
  constructor($ngRedux) {
    'ngInject';

    this.unsubscribe = $ngRedux.connect(this.mapStateToThis, actions)(this);
  }

  $onInit() {
  }

  $onDestroy() {
    this.unsubscribe();
  }

  mapStateToThis(state) {
    const { title, windowState, canNavigate } = state.browserWindow;
    const { canGoBack, canGoForward, isLoading } = state.webContents;
    return {
      canGoBack,
      canGoForward,
      canNavigate,
      title,
      windowState,
      isLoading,
    };
  }

  handleClickProfileButton() {

  }

  handleClickCloseButton() {
    this.closeWindow();
  }

  handleClickIndeterminateButton() {
    this.isMaximized
      ? this.restoreWindow()
      : this.maximizeWindow();
  }

  handleClickMinimizeButton() {
    this.minimizeWindow();
  }

  handleClickBackButton() {
    this.goBack();
  }

  handleClickForwardButton() {
    this.goForward();
  }

  get isMaximized() {
    this.windowState === 'MAXIMIZED';
  }

  get isMinimized() {
    this.windowState === 'MINIMIZED';
  }
}
