import angular from 'angular';

import TrustAsResourceUrl from './trustAsResourceUrl';

export default angular
  .module('electron.filters', [
    TrustAsResourceUrl,
  ])
  .name;
