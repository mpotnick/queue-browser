/**
 * Angular component callback binding type.
 *
 * @type {String}
 */
export const CALLBACK = '&';

/**
 * Angular component string binding type.
 *
 * @type {String}
 */
export const STRING = '@';

/**
 * Angular component one-way binding.
 *
 * @type {String}
 */
export const ONE_WAY = '<';

/**
 * Angular component two-way binding.
 *
 * @type {String}
 */
export const TWO_WAY = '=';
