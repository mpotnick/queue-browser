/*
 * -----------------------------------------------------------------------------
 * Core Module
 * -----------------------------------------------------------------------------
 *
 * This module provides various core application services. By depending on the
 * queue.core module in any other module in this application, you will have
 * access to all of the functionality provided by allother core modules.
 *
 * This module does the job of making sure Angular Material and UI Router are
 * available to the application. Through this module you'll have access to
 * the functionality provided by both of those packages. You will most
 * likely depend on this module in every other module you write.
 *
 */
import angular from 'angular';

import ngMaterial from 'angular-material';
import uiRouter from 'angular-ui-router';
import ngRedux from 'ng-redux';
import ngReduxUiRouter from 'redux-ui-router';
import ngReduxDevTools from 'ng-redux-dev-tools';

import QueueColor from './color';
import QueueIcon from './icon';
import QueueRouter from './router';

import QueueBrowserMiddleware from '../redux/middleware';

import CoreConfig from './core.config';
import CoreRun from './core.run';

// Define the main application module
export default angular
  .module('queue.core', [
    ngMaterial,
    uiRouter,
    ngRedux,
    ngReduxUiRouter,
    ngReduxDevTools,

    QueueBrowserMiddleware,

    QueueColor,
    QueueRouter,
    QueueIcon,

  ])

  // Set up Redux and a few other things
  .config(CoreConfig)
  // .run(CoreRun)
  .name;
