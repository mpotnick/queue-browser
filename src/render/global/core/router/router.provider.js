/*
 * -----------------------------------------------------------------------------
 * Queue Router Provider
 * -----------------------------------------------------------------------------
 *
 * The router provider does the heavy lifting when configuring a new state
 * in an application. To use it, simply call the configureStates method
 * with an array of the states you'd like to configure. It handles
 * adding them to Angular UI Router's $stateProvider for you.
 *
 */

import angular from 'angular';

export default function RouterProvider($locationProvider, $stateProvider, $urlRouterProvider) {
  'ngInject';

  const config = {
    docTitle: undefined,
    resolveAlways: {},
  };

  const stateCounts = {
    errors: 0,
    changes: 0,
  };

  $locationProvider.html5Mode(false);

  this.configure = function configure(cfg) {
    angular.extend(config, cfg);
  };

  function RouterHelper($location, $log, $rootScope, $state) {
    'ngInject';

    let handlingStateChangeError = false;
    let hasOtherwise = false;

    function handleRoutingErrors() {
      // Route cancellation:
      // On routing error, go to the dashboard.
      // Provide an exit clause if it tries to do it twice.
      $rootScope.$on('$stateChangeError', (
        event, toState, toParams, fromState, fromParams, error) => {
        if (handlingStateChangeError) return;

        stateCounts.errors++;
        handlingStateChangeError = true;

        const destination = (toState &&
          (toState.title || toState.name || toState.loadedTemplateUrl)) ||
          'unknown target';

        const msg = `Error routing to ${destination}. ${(error.data || '')}.
          ${(error.statusText || '')}: ${(error.status || '')}`;

        $log.error(msg, [toState]);
        $location.path('/');
      });
    }

    function updateDocTitle() {
      $rootScope.$on('$stateChangeSuccess', (event, toState) => {
        stateCounts.changes++;
        handlingStateChangeError = false;
        const title = `${config.docTitle} ${(toState.title || '')}`;

        $rootScope.title = title; // data bind to <title>
      }
    );
    }

    function init() {
      handleRoutingErrors();
      updateDocTitle();
    }

    init();

    return {
      configureStates(states, otherwisePath) {
        states.forEach((state) => {
          state.config.resolve = angular.extend(state.config.resolve || {}, config.resolveAlways);
          $stateProvider.state(state.state, state.config);
        });
        if (otherwisePath && !hasOtherwise) {
          hasOtherwise = true;
          $urlRouterProvider.otherwise(otherwisePath);
        }
      },

      getStates() {
        return $state.get();
      },

      stateCounts,
    };
  }

  this.$get = RouterHelper;
}
