/*
 * -----------------------------------------------------------------------------
 * Queue Router Helper
 * -----------------------------------------------------------------------------
 *
 * This module makes it easy to add testable states to the ui-router. These
 * routes are added during the run phase of the Angular lifecycle instead
 * of the config phase like usual which allows services and factories
 * to be used during the route configuration process. Very useful.
 *
 */

import angular from 'angular';

import uiRouter from 'angular-ui-router';

import RouterHelperProvider from './router.provider';
import CoreRoutes from './router.route';

export default angular
  .module('queue.router', [
    uiRouter,
  ])
  .provider('RouterHelper', RouterHelperProvider)
  .run(CoreRoutes)
  .name;
