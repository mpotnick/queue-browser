import template from './404.html';

export default {
  state: 'queue.404',
  config: {
    url: '/not-found',
    title: 'Not Found',
    settings: {
      fab: { enabled: false },
      sidebar: {},
      search: { enabled: false },
    },
    views: {
      content: { template },
    },
  },
};
