/*
 * -----------------------------------------------------------------------------
 * Queue Routes
 * -----------------------------------------------------------------------------
 *
 * This defines all of the Queue core routes. This config will handle mapping
 * the 404 state of the application. All requests that cannot be mapped to
 * a state in the app will be mapped here. All the container options such
 * as the container FAB and the search bar can be enabled or disabled
 * on the page also. All of them have been disabled by default.
 *
 */
import notFountState from './404_State';

/**
 * Get the states provided by the application core
 *
 * @return {Array} An array of the routes configured
 */
function getStates() {
  return [
    notFountState,
  ];
}

export default function CoreRoute($http, RouterHelper) {
  'ngInject';

  RouterHelper.configureStates(getStates(), '/');
}
