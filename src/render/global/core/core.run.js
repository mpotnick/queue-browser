/**
 * This is run conditionally if the devToolsExtension is loaded in the current
 * session. The aim is to force angular to render UI updates made soley by
 * way of the devTools cancelling/restoring actions. The UI otherwise
 * will only update after an action is explicitly dispatched
 *
 * @param  {Object} $ngRedux   injected redux angular bindings
 * @param  {Object} $rootScope injected app root scope
 * @return {*}
 */
export default function CoreRun($ngRedux, $rootScope) {
  'ngInject';

  // if (window.devToolsExtension) {
  //   $ngRedux.subscribe(() => {
  //     setTimeout($rootScope.$apply.bind($rootScope), 100);
  //   });
  // }
}
