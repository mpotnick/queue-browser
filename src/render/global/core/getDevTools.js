export default function getDevTools() {
  return window.devToolsExtension ? window.devToolsExtension() : f => f;
}
