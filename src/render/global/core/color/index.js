/*
 * -----------------------------------------------------------------------------
 * Queue Color Provider
 * -----------------------------------------------------------------------------
 *
 * Not the prettiest code, but it's necessary to be able to use the colors
 * defined in Angular Material palettes easily the app template files.
 *
 * Usage:
 * <md-button queue-color="{'background-color': 'Primary.500'}">Button</md-button>
 *
 */

import angular from 'angular';

import Themes from './color';

let _theme;
let _palettes;

function config($mdThemingProvider) {
  $mdThemingProvider.definePalette('queuePrimary', Themes.queue.primary);
  $mdThemingProvider.definePalette('queueAccent', Themes.queue.accent);

  $mdThemingProvider
    .theme('default')
    .primaryPalette('queuePrimary')
    .accentPalette('queueAccent', {
      default: '500',
      'hue-1': 'A100',
      'hue-2': '700',
      'hue-3': '900',
    });

  _theme = $mdThemingProvider.theme();

  _palettes = $mdThemingProvider._PALETTES;
}
config.$inject = ['$mdThemingProvider'];


function queueColor() {
  'ngInject';

  function link(scope, element, attrs) {
    attrs.$observe('queueColor', () => {
      for (const p in scope.queueColor) {
        if (scope.queueColor.hasOwnProperty(p)) {
          const themeColors = _theme.colors;

          const split = (scope.queueColor[p] || '').split('.');

          if (split.length < 2) split.unshift('primary');

          const hueR = split[1] || 'hue-1';                 // 'hue-1'
          const colorR = `queue${split[0]}` || 'primary';  // 'warn'

          // Absolute color: 'orange'
          const colorA = themeColors[colorR] ? themeColors[colorR].name : colorR;

          // Absolute Hue: '500'
          const hueA = themeColors[colorR] ? themeColors[colorR].hues[hueR] || hueR : hueR;

          const colorValue = _palettes[colorA][hueA]
              ? _palettes[colorA][hueA].value
              : _palettes[colorA]['500'].value;

          element.css(p, `rgb(${colorValue.join(',')})`);
        }
      }
    });
  }

  return {
    link,
    restrict: 'A',
    scope: { queueColor: '=' },
  };
}


export default angular
  .module('queue.color', [])
  .config(config)
  .directive('queueColor', queueColor)
  .name;
