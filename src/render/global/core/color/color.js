/*
 * -----------------------------------------------------------------------------
 * Theme Configuration
 * -----------------------------------------------------------------------------
 *
 * This file showcases one of the awesome features webpack enables for us. The
 * variables in this *JavaScript* file are pulled from an *SCSS* variables
 * file. How cool is that? These are the palettes that ngMaterial uses.
 *
 */

import accent from './color--accent.scss';
import primary from './color--primary.scss';

export default {
  queue: {
    accent: {
      50: accent.hue50,
      100: accent.hue100,
      200: accent.hue200,
      300: accent.hue300,
      400: accent.hue400,
      500: accent.hue500,
      600: accent.hue600,
      700: accent.hue700,
      800: accent.hue800,
      900: accent.hue900,
      A100: accent.hueA100,
      A200: accent.hueA200,
      A400: accent.hueA400,
      A700: accent.hueA700,
      contrastDefaultColor: accent.contrastDefaultColor,
    },

    primary: {
      50: primary.hue50,
      100: primary.hue100,
      200: primary.hue200,
      300: primary.hue300,
      400: primary.hue400,
      500: primary.hue500,
      600: primary.hue600,
      700: primary.hue700,
      800: primary.hue800,
      900: primary.hue900,
      A100: primary.hueA100,
      A200: primary.hueA200,
      A400: primary.hueA400,
      A700: primary.hueA700,
      contrastDefaultColor: primary.contrastDefaultColor,
    },
  },
};
