import { SET_APP_BAR } from 'cx-containers/layout/layout.actionTypes';

export default function ToolbarMiddleware() {
  'ngInject';

  return () => next => action => {
    const actionType = '@@reduxUiRouter/$stateChangeStart';

    if (action.type === actionType) {
      const appBarConfig = action.payload.toState.appBar;

      if (appBarConfig) {
        next({ type: SET_APP_BAR, payload: appBarConfig });
      }
    }

    next(action);
  };
}
