/*
 * -----------------------------------------------------------------------------
 * Queue Core Configuration
 * -----------------------------------------------------------------------------
 *
 * Enable debug logging when debug mode is enabled. App debug mode can be
 * enabled by setting the node environment value during the app build.
 *
 */
import effects from 'redux-effects';
import fetch, { fetchEncodeJSON } from 'redux-effects-fetch';
import thunk from 'redux-thunk';
import {
  loadWatcher,
  goWatcher,
  themeColorWatcher,
  windowWatcher,
} from 'cx-electron-utils/lib/redux/middleware/browser';

import rootReducer from '../../reducer';
import createLogger from 'redux-logger';
import getDevTools from './getDevTools';
import ngRedux from 'ng-redux';
import ngReduxDevTools from 'ng-redux-dev-tools';

export default function CoreConfig(
  $locationProvider, $logProvider, $ngReduxProvider, devToolsServiceProvider, $urlRouterProvider) {
  'ngInject';

  $ngReduxProvider.createStoreWith(rootReducer, [
    'cxFirstRunMiddleware',
    loadWatcher,
    goWatcher,
    themeColorWatcher,
    windowWatcher,
    fetchEncodeJSON,
    fetch,
    effects,
    thunk,
    'ngUiRouterMiddleware',
    // createLogger(),
  ], [
    devToolsServiceProvider.instrument(),
  ]);

  if (process.env.DEBUG) {
    $logProvider.debugEnabled(true);
  }

  $urlRouterProvider.otherwise('not-found');
  $locationProvider.html5Mode({ enabled: true, requireBase: false });
}
