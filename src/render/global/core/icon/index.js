/*
 * ----------------------------------------------------------------------------
 * Icon Component
 * ----------------------------------------------------------------------------
 *
 * Provides inline SVGs for shared icons across the user interface.
 *
 * Usage:
 * <cx-icon icon="notify"></cx-icon>
 *
 */

import angular from 'angular';

import 'angular-svg-base-fix';

import { STRING } from '../../bindings';

import controller from './icon.controller';
import template from './icon.html';
import './icon.scss';

const CxIconComponent = {
  bindings: {
    icon: STRING,
  },
  template,
  controller,
};

export default angular
  .module('cx.components.icon', ['svgBaseFix'])
  .component('cxIcon', CxIconComponent)
  .name;
