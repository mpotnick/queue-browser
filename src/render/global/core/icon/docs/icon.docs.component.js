import { BINDINGS } from 'cx-containers/layout/layout.props.js';
import template from './icon.docs.html';
import controller from './icon.docs.controller';
import './icon.docs.scss';

export default {
  bindings: { ...BINDINGS },
  template,
  controller,
};
