import iconHashes from '../icon.hash';

export default class IconDocsController {
  $onInit() {
    this._iconIds = iconHashes.map(file => file.replace(/#(.*?)\..*/ig, '$1'));
  }

  get iconIds() {
    return this._iconIds;
  }
}
