import angular from 'angular';

// import QueueCore from '../../../core';
import IconDocsRoute from './icon.docs.route';
import IconDocsComponent from './icon.docs.component';

export default angular
  .module('queue.icon.docs', [])
  .config(IconDocsRoute)
  .component('documentationIcon', IconDocsComponent)
  .name;
