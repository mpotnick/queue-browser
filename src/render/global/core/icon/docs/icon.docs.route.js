import { ATTRIBUTES } from 'cx-containers/layout/layout.props.js';

export default function IconDocsRoute($stateProvider) {
  'ngInject';

  $stateProvider
    .state('documentationIcon', {
      url: '/documentation/icon',
      template: `
        <documentation-icon
          ${ATTRIBUTES}>
        </documentation-icon>
      `,
    });
}
