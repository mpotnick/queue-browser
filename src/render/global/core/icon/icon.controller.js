/*
 * ----------------------------------------------------------------------------
 * Icon Controller
 * ----------------------------------------------------------------------------
 *
 * Import Icon Data Object, and select the appropriate icon hash and attach it
 * to the controller scope to be used in the template svg use DOM element.
 *
 */

import { contains, find } from 'ramda';

const icons = require.context('./svg', false, /\.svg$/)
const IconHashes = icons.keys().map(icons);

export default class IconController {
  constructor() {
    'ngInject';

    this._iconHash = this.retrieveIconHash(this.icon);
  }

  $onChanges(changes) {
    this._iconHash = this.retrieveIconHash(changes.icon.currentValue);
  }

  retrieveIconHash(icon) {
    return find(contains(`#${icon}.`))(IconHashes);
  }
}
