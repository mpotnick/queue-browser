import defaultState from './state/state';
import { defaultTo } from 'ramda';
import { createAction, createReducer } from 'cx-redux-utils';
import {
} from './layout.setters';

/*
 * -----------------------------------------------------------------------------
 * Hide loading
 * -----------------------------------------------------------------------------
 *
 * @param {Object} payload
 * @return {Object}
 */

export const HIDE_LOADING = '@@cx/layout/hideLoading';

export const hideLoading = createAction(HIDE_LOADING);

export function hideLoadingHandler() {
  return {
    loading: {
      isShown: false,
    },
  };
}

/*
 * -----------------------------------------------------------------------------
 * Hide sidenav
 * -----------------------------------------------------------------------------
 *
 * @param {Object} payload
 * @return {Object}
 */

export const HIDE_SIDENAV = '@@cx/layout/hideSidenav';

export const hideSidenav = createAction(HIDE_SIDENAV);

export function hideSidenavHandler() {
  return {
    sidenav: {
      isOpen: false,
    },
  };
}

/*
 * -----------------------------------------------------------------------------
 * Select dropdown item
 * -----------------------------------------------------------------------------
 *
 * @param {Object} payload
 * @return {Object}
 */

export const SELECT_DROPDOWN_ITEM = '@@cx/layout/selectDropdownItem';

export const selectDropdownItem = createAction(SELECT_DROPDOWN_ITEM);

export function selectDropdownItemHandler(state, action) {
  return dropDownSelectedSetter(action.payload, state);
}


/*
* -----------------------------------------------------------------------------
* Set active tab.
* -----------------------------------------------------------------------------
*
* Set the currently active tab on any page that has a tab bar.
*
*/

export const SET_ACTIVE_TAB = '@@cx/layout/setActiveTab';

export const setActiveTab = createAction(SET_ACTIVE_TAB);

export function setActiveTabHandler(state, action) {
  return activeTabSetter(action.payload, state);
}


/*
* -----------------------------------------------------------------------------
* Set app bar
* -----------------------------------------------------------------------------
*
* Set the app bar config.
*
*/

export const SET_APP_BAR = '@@cx/layout/setAppBar';

export const setAppBar = createAction(SET_APP_BAR);

export function setAppBarHandler(state, action) {
  return {
    appBar: action.payload,
  };
}


/*
 * -----------------------------------------------------------------------------
 * Set dropdown items
 * -----------------------------------------------------------------------------
 *
 * @param {Object} payload
 * @return {Object}
 */

export const SET_DROPDOWN_ITEMS = '@@cx/layout/setDropdownItems';

export const setDropdownItems = createAction(SET_DROPDOWN_ITEMS);

export function setDropdownItemsHandler(state, action) {
  return dropdownItemsSetter(action.payload, state);
}


/*
 * -----------------------------------------------------------------------------
 * Set fab
 * -----------------------------------------------------------------------------
 *
 * @param {Object} payload
 * @return {Object}
 */

export const SET_FAB = '@@cx/layout/setFab';

export const setFab = createAction(SET_FAB);

export function setFabHandler(state, action) {
  const { payload } = action;

  return {
    fab: {
      icon: payload.icon,
      action: payload.action,
      tooltip: payload.tooltip,
    },
  };
}

/*
 * -----------------------------------------------------------------------------
 * Set search
 * -----------------------------------------------------------------------------
 *
 * Set the top bar search field configuration.
 *
 */

export const SET_SEARCH = '@@cx/layout/setSearch';

export const setSearch = createAction(SET_SEARCH);

export function setSearchHandler(state, action) {
  return {
    search: {
      isShown: action.payload.enabled,
    },
  };
}


/*
* -----------------------------------------------------------------------------
* Set selected ids choosen by the select bar
* -----------------------------------------------------------------------------
*
* @param {Object} payload with id1 and id2
* @return {Object}
*/

export const SET_SELECTED_ITEMS = '@@cx/layout/setSelectedItems';

export const setSelectedItems = createAction(SET_SELECTED_ITEMS);

export function setSelectedItemsHandler(state, action) {
  return selectedItemsSetter(action.payload, state);
}

/*
* -----------------------------------------------------------------------------
* Set options for select bar
* -----------------------------------------------------------------------------
*
* @param {Object} payload with options1 and options2
* @return {Object}
*/

export const SET_SELECTIONS = '@@cx/layout/setSelections';

export const setSelections = createAction(SET_SELECTIONS);

export function setSelectionsHandler(state, action) {
  return selectionsSetter(action.payload, state);
}


/*
* -----------------------------------------------------------------------------
* Set the badge for a tab
* -----------------------------------------------------------------------------
*
* @param {Object} payload
* @return {Object}
*/

export const SET_TAB_BADGE = '@@cx/layout/setTabBadge';

export const setTabBadge = createAction(SET_TAB_BADGE);

export function setTabBadgeHandler(state, action) {
  const { payload } = action;
  const tabs = appBarTabsSelector(state);

  return appBarTabsSetter(tabBadgeSetter(payload, tabs), state);
}


/*
 * -----------------------------------------------------------------------------
 * Set title
 * -----------------------------------------------------------------------------
 *
 * Set the top bar page title.
 *
 */

export const SET_TITLE = '@@cx/layout/setTitle';

export const setTitle = createAction(SET_TITLE);

export function setTitleHandler(state, action) {
  return {
    title: {
      text: action.payload.text,
      sref: action.payload.sref,
    },
  };
}


/*
 * -----------------------------------------------------------------------------
 * Show loading
 * -----------------------------------------------------------------------------
 *
 * Show the application loading screen.
 *
 */

export const SHOW_LOADING = '@@cx/layout/showLoading';

export const showLoading = createAction(SHOW_LOADING);

export function showLoadingHandler(state, action) {
  const { isBlocking, theme } = action.payload;
  const defaultPrimary = defaultTo('primary');
  const defaultFalse = defaultTo(false);

  return {
    loading: {
      isBlocking: defaultFalse(isBlocking),
      isShown: true,
      theme: defaultPrimary(theme),
    },
  };
}


/*
* -----------------------------------------------------------------------------
* Show or hide selections header
* -----------------------------------------------------------------------------
*
* @param {Object} payload (which will be true or false)
* @return {Object}
*/

export const SHOW_SELECTIONS = '@@cx/layout/showSelections';

export const showSelections = createAction(SHOW_SELECTIONS);

export function showSelectionsHandler(state, action) {
  return showSelectionsSetter(action.payload, state);
}


/*
 * -----------------------------------------------------------------------------
 * Show sidenav
 * -----------------------------------------------------------------------------
 *
 * Show the application sidenav.
 *
 */

export const SHOW_SIDENAV = '@@cx/layout/showSidenav';

export const showSidenav = createAction(SHOW_SIDENAV);

export function showSidenavHandler() {
  return {
    sidenav: {
      isOpen: true,
    },
  };
}

export const actions = {
  // hideLoading,
  // hideSidenav,
  // selectDropdownItem,
  // setAppBar,
  // setDropdownItems,
  // setFab,
  // setSearch,
  // setTitle,
  // showLoading,
  // showSelections,
  // showSidenav,
};

export default createReducer(defaultState, {
  // [HIDE_LOADING]: hideLoadingHandler,
  // [HIDE_SIDENAV]: hideSidenavHandler,
  // [SELECT_DROPDOWN_ITEM]: selectDropdownItemHandler,
  // [SET_ACTIVE_TAB]: setActiveTabHandler,
  // [SET_APP_BAR]: setAppBarHandler,
  // [SET_DROPDOWN_ITEMS]: setDropdownItemsHandler,
  // [SET_FAB]: setFabHandler,
  // [SET_SEARCH]: setTitleHandler,
  // [SET_SELECTED_ITEMS]: setSelectedItemsHandler,
  // [SET_SELECTIONS]: setSelectionsHandler,
  // [SET_TAB_BADGE]: setTabBadgeHandler,
  // [SET_TITLE]: setSearchHandler,
  // [SHOW_LOADING]: showLoadingHandler,
  // [SHOW_SELECTIONS]: showSelectionsHandler,
  // [SHOW_SIDENAV]: showSidenavHandler,
});
