export default {
  toolbar: {
    isTall: false,
    title: {
      text: '',
      dropdown: {
        isShown: false,
        selected: {},
        items: [],
      },
    },
  },

  tabs: {
    message: '',
    items: [],
  },
};
