import appBar from './state.appBar';
import fab from './state.fab';
import loading from './state.loading';
import sidenav from './state.sidenav';

export default {
  appBar,
  fab,
  loading,
  sidenav,
};
