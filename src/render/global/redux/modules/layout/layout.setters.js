import { createSelector, createSetter } from 'cx-redux-utils';
import { adjust, findIndex, propEq } from 'ramda';

export const dropDownSelectedSetter = createSetter([
  'appBar',
  'toolbar',
  'title',
  'dropdown',
  'selected',
]);

export const appBarTabsSetter = createSetter([
  'appBar',
  'tabs',
  'items',
]);

export const appBarTabsSelector = createSelector([
  'appBar',
  'tabs',
  'items',
]);

export const dropdownItemsSetter = createSetter([
  'appBar',
  'toolbar',
  'title',
  'dropdown',
  'items',
]);

export const activeTabSetter = createSetter([
  'appBar',
  'tabs',
  'selected',
]);

const selectBarPath = ['appBar', 'selectBar'];

export const selectedItemsSetter = createSetter([
  ...selectBarPath,
  'selectedItems',
]);

export const selectionsSetter = createSetter([
  ...selectBarPath,
  'selections',
]);

export const showSelectionsSetter = createSetter([
  ...selectBarPath,
  'show',
]);

export const countSetter = createSetter('count');
export const indexWhereHrefEquals = (sref, tabs) => findIndex(propEq('sref', sref), tabs);

export const tabBadgeSetter = (payload, tabs) =>
  adjust(countSetter(payload.count), indexWhereHrefEquals(payload.sref, tabs), tabs);

export default {
  activeTabSetter,
  dropdownItemsSetter,
  dropDownSelectedSetter,
  selectedItemsSetter,
  selectionsSetter,
};
