import electronJsonStorage from 'electron-json-storage';
import { stateGo } from 'redux-ui-router';

import { setConfig } from '../../../redux/modules/config';

import pkg from '../../../../package.json';


export default function FirstRunMiddleware($sessionStorage, $state) {
  'ngInject';

  return ({ getState }) => next => action => {
    if (!getState().config.domainIsValid && !$state.includes('index.firstRun')) {
      electronJsonStorage.get(
        `${pkg.name}.config`,
        (err, data) => {
          if (err) {
            // todo
          }
          if (!data.domainPrefix || !data.domainIsValid) {
            return next(stateGo('index.firstRun.index'));
          }
          next(setConfig(data));
          next(stateGo('index.webapp'));
        }
      );
    }
    return next(action);
  };
}
