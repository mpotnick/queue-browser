import angular from 'angular';
import ngStorage from 'ngstorage';

import firstRun from './firstRun';

export const CoreMiddleware = [
  firstRun,
];

export default angular
  .module('queueBrowser.core.redux.middleware', [ngStorage.name])
    .factory('cxFirstRunMiddleware', firstRun)
    .name;

