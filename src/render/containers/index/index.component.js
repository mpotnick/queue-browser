import template from './index.html';
import controller from './index.controller.js';

export default {
  template,
  controller,
};
