import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngRedux from 'ng-redux';
import ngAnimate from 'angular-animate';

import IndexComponent from './index.component.js';
import IndexRoute from './index.route.js';
import appBar from '../../components/appBar';

export default angular
  .module('queueIndex.render.containers.index', [
    ngAnimate,
    uiRouter,
    ngRedux,
    appBar,
  ])
  .config(IndexRoute)
  .component('cxIndex', IndexComponent)
  .name;
