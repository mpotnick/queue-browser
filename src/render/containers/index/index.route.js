// import { ATTRIBUTES } from '../../global/layout/layout.props';

export default function WebappRoute($stateProvider) {
  'ngInject';

  $stateProvider
    .state('index', {
      abstract: true,
      template: `
        <cx-index layout="column" layout-fill>
        </cx-index>
      `,
    });
}
