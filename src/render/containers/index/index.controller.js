import color from 'color';
import { remote, ipcRenderer } from 'electron';
import { setIsUpdateDownloaded } from '../../redux/modules/app';

const actions = {
  setIsUpdateDownloaded
};

export default class indexController {
  constructor($ngRedux) {
    'ngInject';

    this.unsubscribe = $ngRedux.connect(this.mapStateToThis, actions)(this);

    ipcRenderer.on('update-downloaded', () => {
      this.setIsUpdateDownloaded(true);
    });
  }

  $onDestroy() {
    this.unsubscribe();
  }

  get isThemeLight() {
    return color(this.themeColor).light();
  }

  mapStateToThis(state) {
    const { themeColor } = state.browserWindow;
    const { domainPrefix } = state.config;
    const { version, arch, platform, isUpdateDownloaded, productName } = state.app;
    return {
      themeColor,
      domainPrefix,
      arch,
      platform,
      version,
      isUpdateDownloaded,
      productName,
    };
  }

  handleClickUpdateButton() {
    remote.autoUpdater.quitAndInstall();
  }
}
