import template from './webapp.html';
import controller from './webapp.controller';

export default {
  template,
  controller,
};
