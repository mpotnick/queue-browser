export default function WebappRoute($stateProvider) {
  'ngInject';

  $stateProvider
    .state('index.webapp', {
      template: `
        <cx-webapp layout="column" flex>
        </cx-webapp>
      `,
    });
}
