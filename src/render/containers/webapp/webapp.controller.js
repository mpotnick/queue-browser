import path from 'path';
import { remote } from 'electron';
import pkg from '../../../../package.json';
import { SEND_IPC_MESSAGE, sendIPCMessage } from 'cx-electron-utils/lib/redux/modules/ipc';
import {
  setIsLoading,
} from 'cx-electron-utils/lib/redux/modules/webContents';
import {
  setTitle,
  processIpcMessage,
  setThemeColor,
} from 'cx-electron-utils/lib/redux/modules/browserWindow';
import { defaultThemeColor } from 'cx-electron-utils/lib/redux/modules/browserWindow/state';
const actions = {
  setTitle,
  processIpcMessage,
  setThemeColor,
  setIsLoading,
  sendIPCMessage,
};

const uaRegex = /(mozilla\/\d(?:\.\d)*)\s+(\((?:windows|macintosh)[^)]*\))/i;

let _userAgent;

export default class WebappController {
  constructor($ngRedux, $sce) {
    'ngInject';
    this.$sce = $sce;
    this.unsubscribe = $ngRedux.connect(this.mapStateToThis, actions)(this);
    this.preloadScriptURI = process.env.NODE_ENV === 'development'
      ? path.resolve(process.cwd(), 'src', 'render', 'bridge.js')
      : path.resolve(process.cwd(), 'resources', `app${pkg.build.asar ? '.asar' : ''}`, `bridge.${__webpack_hash__}.js`);

    const match = uaRegex.exec(remote.getCurrentWebContents().getUserAgent());
    _userAgent = `${match[1]} ${match[2]} ${this.productName}/${this.version} Electron/${process.versions.electron}`;
  }

  $onDestroy() {
    this.unsubscribe();
  }

  get url() {
    return this.$sce.trustAsResourceUrl(this.appUri);
  }

  get userAgent() {
    return _userAgent;
  }

  mapStateToThis(state) {
    const { name, productName, version } = state.app;
    const { domainPrefix } = state.config;
    const { webContents } = state.browserWindow;
    const { navigationIndex } = state.webContents;
    return {
      name,
      appUri: `https://${domainPrefix}.crosschx.com/sso/login?service=${encodeURIComponent(`https://${domainPrefix}.crosschx.com/${name}/`)}`,
      // appUri: 'http://localhost:8080/queue/',
      domainPrefix,
      productName,
      navigationIndex,
      webContents,
      version,
    };
  }

  handleWebviewEvents(event) {
    switch (event.type) {
      case 'ipc-message':
        if (event.channel === 'theme-color') {
          let { electronColor } = event.args[0];
          switch (electronColor) {
            case 'md-accent':
              electronColor = '#004e7c';
                break;
            case 'md-primary':
            default:
              electronColor = defaultThemeColor;
          }
          this.sendIPCMessage({ delegateAction: this.setThemeColor(electronColor) });
        } else {
          const { delegateAction } = event;
          this.sendIPCMessage({ delegateAction });
        }
        break;
      case 'did-start-loading':
        this.setIsLoading(
          {
            isLoading: true,
          },
          {
            error: false,
          }
        );
        break;
      case 'did-stop-loading':
        this.setIsLoading(
          {
            isLoading: false,
          },
          {
            error: false,
          }
        );
        break;
      case 'did-fail-load': {
        const { errorCode, errorDescription, isMainFrame, validatedURL } = event;
        this.setIsLoading(
          {
            isLoading: false,
          },
          {
            error: {
              errorCode,
              errorDescription,
              isMainFrame,
              validatedURL,
            },
          }
        );
        // todo: show error?
        break;
      }
      // case 'did-navigate':
      //   console.log(event.url);
      //   break;
      // case 'did-navigate-in-page':
      //   break;
      // case 'ipc-message': {
      //   const { channel, payload } = event;
      //   this.processIpcMessage({
      //     channel,
      //     payload,
      //   });
      //   break;
      // }
      case 'page-title-updated':
        if (event.explicitSet) {
          this.setTitle(event.title);
        }
        break;
      // case 'did-change-theme-color':
      //   this.setThemeColor(event.themeColor);
      //   break;
      // case 'did-get-redirect-request': {
      //   const { oldURL, newURL, isMainFrame } = event;
      //   console.log({ oldURL, newURL, isMainFrame });
      //   break;
      // }
      // case 'did-get-response-details': {
      //   const {
      //     status,
      //     newURL,
      //     originalURL,
      //     httpResponseCode,
      //     requestMethod,
      //     referrer,
      //     headers,
      //     resourceType,
      //   } = event;
      //   if (resourceType === 'mainFrame') {
      //     // todo: modify headers?
      //   }
      //   break;
      // }
      default:
        return;
    }
  }
}
