import { setWebContents } from 'cx-electron-utils/lib/redux/modules/browserWindow';
import { SEND_IPC_MESSAGE } from 'cx-electron-utils/lib/redux/modules/ipc';
import { CALLBACK } from '../../global/bindings';

export const events = [
  'load-commit',
  'did-finish-load',
  'did-fail-load',
  'did-frame-finish-load',
  'did-start-loading',
  'did-stop-loading',
  'did-get-response-details',
  'did-get-redirect-request',
  'dom-ready',
  'page-title-updated',
  'page-favicon-updated',
  'enter-html-full-screen',
  'leave-html-full-screen',
  'console-message',
  'found-in-page',
  'new-window',
  'will-navigate',
  'did-navigate',
  'did-navigate-in-page',
  'close',
  'ipc-message',
  'crashed',
  'gpu-crashed',
  'plugin-crashed',
  'destroyed',
  'did-change-theme-color',
  'update-target-url',
];

export default ($ngRedux) => {
  'ngInject';

  return {
    restrict: 'A',
    scope: {
      cxWebviewHandler: CALLBACK
    },
    link: (scope, iElement) => {
      // initial load that sets the webContents
      iElement.one('did-start-loading', (e) => {
        const webContents = e.target.getWebContents();
        $ngRedux.dispatch(
          setWebContents({ webContents })
        );
        if (process.env.DEBUG) {
          webContents.openDevTools();
        }
      });

      events.forEach(e => {
        iElement.on(e, handleWebviewEvent);
      });

      scope.$on('$destroy', () => {
        events.forEach(e => {
          iElement.off(e, handleWebviewEvent);
        });
      });

      function handleWebviewEvent(event) {
        scope.cxWebviewHandler({ event });
      }
    }
  };

};
