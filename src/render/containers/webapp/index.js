import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngRedux from 'ng-redux';

import WebappComponent from './webapp.component';
import WebviewHandler from './webviewHandler.directive';
import WebappRoute from './webapp.route';

export default angular
  .module('queueBrowser.render.containers.webapp', [
    uiRouter,
    ngRedux,
  ])
  .config(WebappRoute)
  .directive('cxWebviewHandler', WebviewHandler)
  .component('cxWebapp', WebappComponent)
  .name;
