import { stateGo } from 'redux-ui-router';

import { checkDomainPrefix, checkDomainUri } from '../../redux/modules/config';
import './firstRun.scss';

const actions = {
  checkDomainPrefix,
  checkDomainUri,
  stateGo,
}

export default class firstRunController {
  constructor($ngRedux) {
    'ngInject';
    this.unsubscribe = $ngRedux.connect(this.mapStateToThis, actions)(this);
  }

  $onInit() {

  }

  $onDestroy(){
    this.unsubscribe();
  }

  mapStateToThis(state) {
    const { domainPrefix, domainIsValid } = state.config;
    return {
      domainPrefix,
      domainIsValid,
    };
  }

  handleSubmitDomainForm(domainPrefix, $event) {
    this.checkDomainPrefix(domainPrefix)
      .then(() => this.checkValidity())
      .then(() => {
        this.stateGo('^.complete');
      })
      .catch(() => {
      });
  }

  checkValidity() {
    if (!this.domainIsValid) {
      this.domainForm.domain.$setValidity('queueId', false);
      return Promise.reject();
    }
    this.domainForm.domain.$setValidity('queueId', true);
    return Promise.resolve();
  }
}
