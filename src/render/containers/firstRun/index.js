import angular from 'angular';
import ngMessages from 'angular-messages';
import ngAnimate from 'angular-animate';

import firstRunComponent from './firstRun.component';
import modelLowercase from './modelLowercase.directive';
import route from './firstRun.route';


export default angular
  .module('queueBrowser.containers.firstRun', [ngAnimate, ngMessages])
  .directive('cxModelLowercase', modelLowercase)
  .component('cxFirstRun', firstRunComponent)
  .config(route)
  .name;
