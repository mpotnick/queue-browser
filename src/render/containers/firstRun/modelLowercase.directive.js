import angular from 'angular';

export default ($animate) => {
  'ngInject';

  return {
    restrict: 'A',
    require: 'ngModel',
    link: (scope, iElement, iAttrs, modelCtrl) => {
      modelCtrl.$parsers.push( (inputValue) => {
        const transformedInput = inputValue.toLowerCase().replace(/ /g, '');

        if (transformedInput !== inputValue) {
          modelCtrl.$setViewValue(transformedInput);
          modelCtrl.$render();
        }

        return transformedInput;
      });
      modelCtrl.$viewChangeListeners.push( () => {
        modelCtrl.$setValidity('queueId', true);
      });
    }
  };
};
