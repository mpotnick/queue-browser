import template from './firstRun.html';
import controller from './firstRun.controller';

export default {
  template,
  controller,
};
