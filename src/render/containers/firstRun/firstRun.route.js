import index from './firstRun.index.html';
import domain from './firstRun.domain.html';
import complete from './firstRun.complete.html';

export default function WebappRoute($stateProvider) {
  'ngInject';

  $stateProvider
    .state('index.firstRun', {
      abstract: true,
      template: `
        <cx-first-run class="bg--cyan-200" layout="column" layout-fill>
        </cx-first-run>
      `,
    })
    .state('index.firstRun.index', {
      template: index,
    })
    .state('index.firstRun.domain', {
      template: domain,
    })
    .state('index.firstRun.complete', {
      template: complete,
    })
    ;
}
