import angular from 'angular';
import ngMaterial from 'angular-material';
import CxCore from './global/core';
import CxIndex from './containers/index';
import CxWebapp from './containers/webapp';
import CxFirstRun from './containers/firstRun';

import './index.scss';
import './roboto.scss';
import './proxima-nova.scss';

if (process.env.DEBUG) {
  require('ng-redux-dev-tools/src/index.scss');
}

angular
  .module('queueBrowser', [
    CxCore,
    CxIndex,
    CxWebapp,
    CxFirstRun,
  ]);
