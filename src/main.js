/* eslint-disable max-len, no-unused-vars */
import * as electronSquirrelStartup from 'electron-squirrel-startup';
import os from 'os';
import { createStore, applyMiddleware, compose } from 'redux';
import { dialog, BrowserWindow, app, Menu, autoUpdater, ipcMain } from 'electron';
import path from 'path';
import pkg from './package.json';

import windowConfig from './main/config/browserWindow.config';
import applicationMenu from './main/menus/applicationMenu';
import downloadDialog from './main/dialogs/download';
import {
  maximizeWindow,
  minimizeWindow,
  restoreWindow,
} from 'cx-electron-utils/lib/redux/modules/browserWindow';
import {
  goForward,
  goBack,
} from 'cx-electron-utils/lib/redux/modules/webContents';

// import { electronEnhancer } from 'redux-electron-store';
// import middleware from './main/redux/middleware';
// import reducer from './main/redux/reducer';

if (process.env.DEBUG) {
  require('electron-debug')();
}

// const enhancer = compose(
//   applyMiddleware(...middleware),
//   electronEnhancer()
// );
// export const store = createStore(reducer, initialState, enhancer);

let mainWindow = null;

const singleton = app.makeSingleInstance((argv, workingDirectory) => {
  if (mainWindow) {
    if (mainWindow.isMinimized()) mainWindow.restore();
    mainWindow.focus();
  }
});

(function instanceCheck(){
  if (singleton) {
    app.quit();
    return;
  }
})();

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.

// app.commandLine.appendSwitch('ignore-certificate-errors', true);

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow(windowConfig);

  downloadDialog.on(mainWindow);

  // and load the index.html of the app
  mainWindow.loadURL(process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : `file://${path.join(__dirname, 'index.html')}`);

  mainWindow.on('close', (event) => {
    // downloadDialog.off(event.target);
  });

  // Emitted when the window is closed.
  mainWindow.on('closed', (event) => {

    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });

  // mainwindow.on('minimize', (event) => {
  //   store.dispatch(minimizeWindow);
  // });
  //
  // mainwindow.on('restore', (event) => {
  //   store.dispatch(restoreWindow);
  // });
  //
  // mainwindow.on('maximize', (event) => {
  //   store.dispatch(maximizeWindow);
  // });
  //
  // mainwindow.on('unmaximize', (event) => {
  //   store.dispatch(restoreWindow);
  // });
}

function setupApplicationMenu() {
  const menu = Menu.buildFromTemplate(applicationMenu);
  Menu.setApplicationMenu(Menu.buildFromTemplate(applicationMenu));
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', () => {

  setupApplicationMenu();
  createWindow();

  // Squirrel needs to update the Windows app and will quit automatically
  if (electronSquirrelStartup === true) {
    return null;
  }

  try{
    autoUpdater.setFeedURL(`https://s3.amazonaws.com/queue-browser/${os.platform()}/${os.arch()}/updates/`);
    autoUpdater.checkForUpdates();
  } catch (e) {
    console.error(e.message);
    console.info('This is expected if you\'re on OSX and your application isn\'t signed. See: https://discuss.atom.io/t/problem-with-auto-updater/14537');
  }

});

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  app.quit();
});

autoUpdater.on('checking-for-update', () => {

});

autoUpdater.on('update-available', () => {
  return true;
});

autoUpdater.on('update-not-available', () => false);

autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName, releaseDate, updateURL) => {

  mainWindow.webContents.send('update-downloaded', {event, releaseNotes, releaseName, releaseDate, updateURL} );

});
