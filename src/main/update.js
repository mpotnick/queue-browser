import { app } from 'electron';
import { spawn } from 'child_process';
import * as path from 'path';

function run(args, done) {
  const appFolder = path.resolve(process.execPath, '..');
  const rootAtomFolder = path.resolve(appFolder, '..');
  const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'));

  console.log(`Spawning ${updateDotExe} with args ${args}`);
  spawn(updateDotExe, args,
    {
      detached: true,
    }
  )
  .on('close', done);
}

export default function handleStartupEvent() {
  if (process.platform !== 'win32') {
    return false;
  }
  const cmd = process.argv[1]
  console.log(`Processing squirrel command ${cmd}`);
  const target = path.basename(process.execPath);

  switch (cmd) {
    case '--squirrel-install':
    case '--squirrel-updated':
      run([`--createShortcut=${target}`], app.quit);
      setTimeout(app.quit, 1000);
      return true;
    case '--squirrel-uninstall':
      run([`--removeShortcut=${target}`], app.quit);
      setTimeout(app.quit, 1000);
      return true;
    case '--squirrel-obsolete':
      app.quit();
      return true;
    default:
      return false;
  }
}
