import pkg from '../../../package.json';

import { app } from 'electron';

const menu = [
  {
    label: pkg.productName,
    submenu: [
      {
        label: `About ${pkg.productName}`,
        role: 'about',
      },
      {
        type: 'separator',
      },
      {
        label: 'Services',
        role: 'services',
        submenu: [],
      },
      {
        type: 'separator',
      },
      {
        label: `Hide ${pkg.productName}`,
        accelerator: 'Command+H',
        role: 'hide',
      },
      {
        label: 'Hide Others',
        accelerator: 'Command+Alt+H',
        role: 'hideothers',
      },
      {
        type: 'separator',
      },
      {
        label: 'Quit',
        accelerator: 'Command+Q',
        click() { app.quit(); },
      },
      {
        type: 'separator',
      },
      {
        label: `${pkg.productName} Version ${pkg.version}`
      }
    ],
  },
  null,
  null,
  {
    submenu: [
      {
        type: 'separator',
      },
      {
        label: 'Bring All to Front',
        role: 'front',
      },
    ],
  },
];

export default module.exports = config => {
  config.unshift(menu[0]);
  config[3].submenu.push(menu[3].submenu[0]);

  if (process.env.DEBUG) {
    config[1].submenu.push(
      {
        label: 'Toggle Chrome Developer Tools',
        accelerator: 'Alt+Command+I',
        click(item, focusedWindow) {
          if (focusedWindow) {
            focusedWindow.toggleDevTools();
          }
        },
      }
    );
  }
  return config;
};
