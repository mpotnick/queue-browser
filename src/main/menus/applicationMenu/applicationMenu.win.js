import pkg from '../../../package.json';

export default module.exports = config => {
  config[2].submenu.push(
    {
      type: 'separator',
    },
    {
      label: `${pkg.productName} Version ${pkg.version}`
    }
  );
  if (process.env.DEBUG) {
    config[1].submenu.push(
      {
        label: 'Toggle Chrome Developer Tools',
        accelerator: 'Ctrl+Shift+I',
        click(item, focusedWindow) {
          if (focusedWindow) focusedWindow.toggleDevTools();
        },
      }
    );
  }
  return config;
};
