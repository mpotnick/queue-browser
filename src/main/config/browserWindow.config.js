import pkg from '../../package.json';
import os from 'os';

const config = {
  title: pkg.productName,
  width: 1024,
  height: 768,
  center: false,
  frame: false,
  webPreferences: {
    javascript: true,
    plugins: true,
    nodeIntegration: true,
    defaultFontFamily: {
      sansSerif: 'Roboto',
      serif: 'Roboto Slab',
      standard: 'Roboto',
    },
  },
};

switch (os.platform()) {
  case 'darwin':
    Object.assign(config, require('./browserWindow.config.osx'));
    break;
  case 'win32':
  default:
    Object.assign(config, require('./browserWindow.config.win'));
    break;
}

export default config;
