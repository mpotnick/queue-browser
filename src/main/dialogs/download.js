import path from 'path';
import { map } from 'ramda';
import { app, dialog, DownloadItem } from 'electron';
import * as mimeTypes from 'mime-types';

function handleWillDownload(event, item, webContents) {
  const fileName = item.getFilename();
  const filters = map(
    (val) => ({ name: `.${val} File`, extensions: [val] }), mimeTypes.extensions[item.getMimeType()]
  );
  const file = dialog.showSaveDialog({
    defaultPath: path.join(app.getPath('downloads'), fileName),
    filters,
  });

  if (file) {
    item.setSavePath(file);
  } else {
    item.cancel();
  }
}

export default {
  on: (browserWindow) => {
    browserWindow.webContents.session.on('will-download', handleWillDownload);
  },
  off: (browserWindow) => {
    browserWindow.webContents.session.off('will-download', handleWillDownload);
  },
};
