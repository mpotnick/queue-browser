# Queue Browser

### An [Electron](http://electron.atom.io)-based native desktop application for Queue 2.0

![Yo dawg](./x.png "Optional title")


Setup
-----

Before starting, make sure you have Node v >=5 installed


Next you'll need to [follow the instructions here](https://github.com/electron-userland/electron-builder/wiki/Multi-Platform-Build) regarding multi-platform builds for your development environment.  
The following assumes you're on OSX.

- Install prereqs for Windows packaging:
```
brew install Caskroom/cask/xquartz wine mono
```

- (Optional) Install prereqs for Linux packaging:
```
brew install gnu-tar libicns graphicsmagick
```

- (Optional) Install prereqs for RPM packaging:
```
brew install rpm
```



This project uses `electron-builder` to build and package the application into distributable formats. The [suggested setup](https://github.com/electron-userland/electron-builder#two-packagejson-structure), as of June 2016, is to use a two-`package.json`-setup: one for the build system and dev tool dependencies and one for published application requirements. This provides a nice, declarative separation of dependencies.
 
The root `package.json` is referred as the "development" package and the one in `./app` is the "application" package.  

To get started, install your Node dependencies for each `package.json`:
```
npm install && cd ./src && npm install 
```

Tasks are listed in the `scripts` object of the development `package.json`. The primary tasks are:

`npm start` or `npm run dev`: Start up development inside of an Electron instance.  
`npm run build`: Build the application into `./app` using Webpack.  
`npm run dist`: Build the application and package into all distributable formats.

Generally this means you'd run the following to produce a normal Windows build:  
`npm run build && npm run dist:win`

This will build the project and attempt to produce a Windows build. I say "attempt", because there are a few things to be aware of: `electron-builder` does a few important things behind the scenes, and they depend on the build targets and build environment:

- The Windows app is signed, and it needs a few environment variables set for this to work properly:  
    1. Grab the `.p12` from from [here](https://bitbucket.org/crosschx/code-signing-certification/src/072fdaed0226c9d1aea9f5ded5b9d0eb9bdc0072/2015-10-23/?at=master) and convert it into a base64-encoded string using `base64 -i codesigncert.p12 -o output.txt`
    2. Copy the content of `output.txt` and save it as an environment var named `CSC_LINK`  
    3. Copy the `# Export Key Pair (*.p12)` password from [here](https://bitbucket.org/crosschx/code-signing-certification/src/072fdaed0226c9d1aea9f5ded5b9d0eb9bdc0072/2015-10-23/certificate_generation.txt?at=master&fileviewer=file-view-default) and save it as an environment variable named `CSC_KEY_PASSWORD`.  
- Part of Squirrel's responsibility in the update process is generating patch files for upgrading, which means it needs to fetch the previous app from S3 to produce the diff'd artifacts. Version numbering is important; if you need to build a new version, you need to make sure to bump the `package.json` versions or the package will fail.
- If you're building on a Mac, you can still produce a Windows build, but you cannot produce an `.msi` artifact